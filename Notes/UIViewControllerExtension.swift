//
//  UIViewControllerExtension.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 01/09/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import UIKit

// Расширение с методом для создания уедомления с текстом и одной кнопкой "ОК"

extension UIViewController {
    func showAlert(title: String, message: String, handler: @escaping () -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {_ in
            handler()
        }))
        self.present(alert, animated: true, completion: nil)
    }
}
