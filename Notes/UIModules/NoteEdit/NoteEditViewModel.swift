//
//  NoteEditViewModel.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 26/08/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import Foundation
import ReactiveSwift

struct NoteEditViewModel {
    
    var uid: MutableProperty<String>
    var title: MutableProperty<String>
    var content: MutableProperty<String>
    var color: MutableProperty<UIColor>
    var importance: MutableProperty<Importance>
    var selfDestructDate: MutableProperty<Date?>
    
    var note: Note {
        return Note(uid: self.uid.value,
                    title: self.title.value,
                    content: self.content.value,
                    color: self.color.value,
                    importance: self.importance.value,
                    selfDestructDate: self.selfDestructDate.value)
    }
    
    init(note: Note) {
        self.uid = MutableProperty(note.uid)
        self.title = MutableProperty(note.title)
        self.content = MutableProperty(note.content)
        self.color = MutableProperty(note.color)
        self.importance = MutableProperty(note.importance)
        self.selfDestructDate = MutableProperty(note.selfDestructDate)
    }
}
