//
//  NoteEditRouter.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 30/08/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import UIKit

protocol NoteEditRouterProtocol {
    func pop()
    func showColorPicker(with color: [CGFloat], saveColor: @escaping ([CGFloat]) -> Void)
}

class NoteEditRouter: NoteEditRouterProtocol {
    
    private weak var noteEditViewController: UIViewController!
    
    init(noteEditViewController: UIViewController) {
        self.noteEditViewController = noteEditViewController
    }
    
    func pop() {
        noteEditViewController.navigationController?.popViewController(animated: true)
    }
    
    func showColorPicker(with color: [CGFloat], saveColor: @escaping ([CGFloat]) -> Void) {
        let colorPickerController = ColorPickerViewController()
        colorPickerController.configurate(color: color, saveColor: saveColor)
        noteEditViewController.navigationController?.pushViewController(colorPickerController, animated: true)
    }
}
