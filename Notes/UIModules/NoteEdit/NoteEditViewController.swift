//
//  NoteEditViewController.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 03/07/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import UIKit
import ReactiveCocoa
import ReactiveSwift

class NoteEditViewController: UIViewController, UITextFieldDelegate,
                              UIGestureRecognizerDelegate {

    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var destructDatePickerContainer: UIView!
    @IBOutlet weak var destructDatePicker: UIDatePicker!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var noteTitleTextField: UITextField!
    @IBOutlet weak var noteContentTextView: UITextView!
    @IBOutlet weak var whiteTile: ColorView!
    @IBOutlet weak var redTile: ColorView!
    @IBOutlet weak var greenTile: ColorView!
    @IBOutlet weak var editableTile: ColorView!
    @IBOutlet weak var datePickerSwitch: UISwitch!
    
    private var noteViewModel: NoteEditViewModel!
    private var noteEditRouter: NoteEditRouterProtocol!
    private var saveNote: ((Note) -> Void)!
    

    private var keyboardHeight: CGFloat = 0
    
    @IBAction func onLongTapOnTile(_ sender: UILongPressGestureRecognizer) {
        if sender.state == .began {
            noteEditRouter.showColorPicker(with: getColorHsb(editableTile.color), saveColor: setColor(_:))
        }
    }
    
    @IBAction func selectTile(_ sender: UITapGestureRecognizer) {
        guard let tile = sender.view as? ColorView else { return }
        guard let color = tile.color else { return }
        deselectAllTiles()
        tile.isSelected = true
        noteViewModel.color.value = color
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bindModelToView()
        bindViewToModel()
        
        navigationItem.leftBarButtonItem =
            UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelButtonAction))
        navigationItem.leftBarButtonItem?.tintColor = .red
        navigationItem.rightBarButtonItem =
            UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveButtonAction))
        
        bindSaveButtonToText()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onKeyboardShown),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onKeyboardHided),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        noteTitleTextField.delegate = self
    }
    
    func configurate(note: Note, title: String, saveNote: @escaping (Note) -> Void) {
        self.title = title
        self.noteViewModel = NoteEditViewModel(note: note)
        self.noteEditRouter = NoteEditRouter(noteEditViewController: self)
        self.saveNote = saveNote
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        dismissKeyboard()
        return true
    }

    private func bindModelToView() {
        noteTitleTextField.reactive.text <~ noteViewModel.title
        noteContentTextView.reactive.text <~ noteViewModel.content
        
        noteViewModel.color.producer.startWithValues { [weak self] (color) in
            guard let self = self else { return }
            self.deselectAllTiles()
            switch color {
            case let c where c == UIColor.white:
                self.whiteTile.isSelected = true
                break
            case let c where c == UIColor.red:
                self.redTile.isSelected = true
                break
            case let c where c == UIColor.green:
                self.greenTile.isSelected = true
                break
            default:
                let colorHsb = self.getColorHsb(color)
                self.editableTile.isDarkColor = colorHsb[2] < 0.5 ? true : false
                self.editableTile.color = color
                self.editableTile.isSelected = true
                break
            }
        }
        noteViewModel.selfDestructDate.producer.startWithValues { [weak self] (date) in
            guard let self = self else { return }
            let animated = (self.view.window == nil) ? false : true
            if let date = date {
                self.setDatePickerIsHidden(false, animated: animated)
                self.destructDatePicker.date = date
            } else {
                self.setDatePickerIsHidden(true, animated: animated)
            }
        }
    }
    
    private func bindViewToModel() {
        noteViewModel.title <~ noteTitleTextField.reactive.continuousTextValues
        noteViewModel.content <~ noteContentTextView.reactive.continuousTextValues
        noteViewModel.selfDestructDate <~ datePickerSwitch.reactive.isOnValues.map {
            $0 ? self.destructDatePicker.date : nil
        }
    }
    
    private func bindSaveButtonToText() {
        // Бинд isEnabled к текстовому содержимому заметки
        // Если и заголовок, и содержимое заметки пусто, то сохранить заметку невозможно
        navigationItem.rightBarButtonItem?.reactive.isEnabled <~ noteViewModel.title.map {
            !$0.isEmpty || !self.noteViewModel.content.value.isEmpty
        }
        navigationItem.rightBarButtonItem?.reactive.isEnabled <~ noteViewModel.content.map {
            !$0.isEmpty || !self.noteViewModel.title.value.isEmpty
        }
    }
    
    @objc private func onKeyboardShown(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey]
            as? NSValue)?.cgRectValue {
            keyboardHeight = keyboardSize.size.height
            var contentInset: UIEdgeInsets = self.scrollView.contentInset
            contentInset.bottom = keyboardSize.size.height
            scrollView.contentInset = contentInset
        }
    }
    
    @objc private func onKeyboardHided(notification: NSNotification) {
        let contentInset: UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
    
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc private func cancelButtonAction() {
        noteEditRouter.pop()
    }
    
    @objc private func saveButtonAction() {
        saveNote(noteViewModel.note)
        noteEditRouter.pop()
    }
    
    private func setDatePickerIsHidden(_ isHidden: Bool, animated: Bool) {
        guard destructDatePickerContainer != nil, stackView != nil, scrollView != nil else {
            return
        }
        let duration: Double = animated ? 1 : 0
        // Открытие datePicker'а с перемещением offset'а scrollView, чтобы datePicker после открытия не уезжал вниз
        datePickerSwitch.isOn = !isHidden
        UIView.animate(withDuration: duration) {
            let containerHeight = self.destructDatePickerContainer.frame.height
            self.destructDatePickerContainer.isHidden = isHidden
            self.stackView.layoutIfNeeded()
            var newOffset = self.scrollView.contentOffset
            newOffset.y += self.destructDatePickerContainer.frame.height - containerHeight
            self.scrollView.setContentOffset(newOffset, animated: false)
        }
    }
    
    private func deselectAllTiles() {
        whiteTile.isSelected = false
        redTile.isSelected = false
        greenTile.isSelected = false
        editableTile.isSelected = false
    }
    
    private func getColorHsb(_ color: UIColor?) -> [CGFloat] {
        guard let color = color else {
            return [0.5, 0.5, 1]
        }
        var h: CGFloat = 0
        var s: CGFloat = 0
        var b: CGFloat = 0
        color.getHue(&h, saturation: &s, brightness: &b, alpha: nil)
        return [h, 1 - s, b]
    }
    
    private func setColor(_ color: [CGFloat]) {
        noteViewModel.color.value = UIColor(hue: color[0],
                                      saturation: 1 - color[1],
                                      brightness: color[2],
                                      alpha: 1)
    }
}
