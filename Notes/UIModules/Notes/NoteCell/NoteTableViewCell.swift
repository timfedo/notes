//
//  NoteTableViewCell.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 17/07/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import UIKit

class NoteTableViewCell: UITableViewCell {

    @IBOutlet weak var noteColorView: UIView!
    @IBOutlet weak var noteTitleLabel: UILabel!
    @IBOutlet weak var noteContentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        noteColorView.layer.borderColor = UIColor.black.cgColor
        noteColorView.layer.borderWidth = 1
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        let color = noteColorView.backgroundColor
        super.setHighlighted(highlighted, animated: animated)
        if highlighted {
            noteColorView.backgroundColor = color
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        let color = noteColorView.backgroundColor
        super.setSelected(selected, animated: animated)
        if selected {
            noteColorView.backgroundColor = color
        }
    }
}
