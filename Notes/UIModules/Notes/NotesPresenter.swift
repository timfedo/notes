//
//  NotesViewPresenter.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 21/08/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import Foundation

protocol NotesPresenterProtocol {
    var notesCount: Int { get }
    func editButtonTapped(currentIsEditing: Bool)
    func newNoteTapped()
    func updateData()
    func noteDeleted(at indexPath: IndexPath)
    func noteSelected(at indexPath: IndexPath)
    func getNote(at indexPath: IndexPath) -> Note
    func viewLoaded()
}

class NotesPresenter: NotesPresenterProtocol {
    
    private weak var notesCoordinator: NotesViewControllerProtocol!
    private var notesInteructor: NotesInteructorProtocol!
    private var notesRouter: NotesRouterProtocol!
    
    var notesCount: Int {
        get {
            return notesInteructor.notes.count
        }
    }
    private var selectedIndex: Int?
    
    init(notesController: NotesViewControllerProtocol,
         notesInteructor: NotesInteructorProtocol,
         notesRouter: NotesRouterProtocol) {
        self.notesCoordinator = notesController
        self.notesRouter = notesRouter
        self.notesInteructor = notesInteructor
        notesInteructor.setNoAuthHandler {
            DispatchQueue.main.async {
                notesRouter.showAuthorizationView(completion: self.updateData)
            }
        }
        notesInteructor.setFailureHandler {
            DispatchQueue.main.async {
                notesRouter.showAlert(title: "Нет интернет соединения",
                          message: "Вам будет доступно редактирование заметок оффлайн, но при следующем подключении к сети заметки будут перезаписаны заметками с сервера") { }
            }
        }
    }
    
    func editButtonTapped(currentIsEditing: Bool) {
        notesCoordinator.setTableEditing(to: !currentIsEditing)
    }
    
    func newNoteTapped() {
        selectedIndex = nil
        notesRouter.showNoteEditView(note: Note(title: "",
                                                content: "",
                                                importance: .regular,
                                                selfDestructDate: nil),
                                     navigationTitle: "Создание заметки",
                                     saveNote: noteAdded(_:))
    }
    
    func updateData() {
        notesInteructor.loadNotes {
            self.notesCoordinator.updateTable()
        }
    }
    
    func noteDeleted(at indexPath: IndexPath) {
        notesInteructor.removeNote(at: reverseIndex(indexPath.row))
        notesCoordinator.deleteRow(at: indexPath)
    }
    
    func noteSelected(at indexPath: IndexPath) {
        selectedIndex = indexPath.row
        notesCoordinator.dismissHighlight(at: indexPath)
        notesRouter.showNoteEditView(note: notesInteructor.notes[reverseIndex(indexPath.row)],
                                     navigationTitle: "Изменение заметки",
                                     saveNote: noteAdded(_:))
    }
    
    private func noteAdded(_ note: Note) {
        if let index = self.selectedIndex {
            notesInteructor.updateNote(note, at: reverseIndex(index))
            notesCoordinator.updateOldNote(at: IndexPath(row: index, section: 0))
        } else {
            notesInteructor.addNewNote(note)
            notesCoordinator.insertNewNote()
        }
    }
    
    func getNote(at indexPath: IndexPath) -> Note {
        return notesInteructor.notes[reverseIndex(indexPath.row)]
    }
    
    private func reverseIndex(_ index: Int) -> Int {
        return notesCount - index - 1
    }
    
    func viewLoaded() {
        updateData()
    }
}
