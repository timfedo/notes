//
//  ViewController.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 27/06/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import UIKit
import CoreData

protocol NotesViewControllerProtocol: class {
    func updateTable()
    func setTableEditing(to isEditing: Bool)
    func deleteRow(at indexPath: IndexPath)
    func dismissHighlight(at indexPath: IndexPath)
    func insertNewNote()
    func updateOldNote(at indexPath: IndexPath)
}

class NotesViewController: UIViewController {
    
    private var notesPresenter: NotesPresenterProtocol!
    
    @IBOutlet weak var notesTableView: UITableView!
    
    private let reuseIdentifier = "note cell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        notesPresenter.viewLoaded()
        
        title = "Заметки"
        navigationItem.leftBarButtonItem =
            UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(editButtonAction))
        navigationItem.rightBarButtonItem =
            UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addButtonAction))
        
        notesTableView.refreshControl = UIRefreshControl()
        notesTableView.refreshControl?.addTarget(self, action: #selector(refreshControlAction), for: .valueChanged)
        notesTableView.refreshControl?.beginRefreshing()
        
        notesTableView.register(UINib(nibName: "NoteTableViewCell", bundle: nil),
                                forCellReuseIdentifier: reuseIdentifier)
    }
    
    func configurate(backgroundContext: NSManagedObjectContext) {
        let notesInteructor = NotesInteructor(backgroundContext: backgroundContext)
        let notesRouter = NotesRouter(notesController: self)
        notesPresenter =
            NotesPresenter(notesController: self, notesInteructor: notesInteructor, notesRouter: notesRouter)
    }
    
    @objc private func editButtonAction() {
        notesPresenter.editButtonTapped(currentIsEditing: notesTableView.isEditing)
    }
    
    @objc private func addButtonAction() {
        notesPresenter.newNoteTapped()
    }
    
    @objc private func refreshControlAction() {
        notesPresenter.updateData()
    }
}

extension NotesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notesPresenter.notesCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let note = notesPresenter.getNote(at: indexPath)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! NoteTableViewCell
        
        cell.noteColorView?.backgroundColor = note.color
        cell.noteTitleLabel?.text = note.title
        cell.noteContentLabel?.text = note.content
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
            notesPresenter.noteDeleted(at: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        notesPresenter.noteSelected(at: indexPath)
    }
}

extension NotesViewController: NotesViewControllerProtocol {
    
    func updateTable() {
        notesTableView.reloadData()
        notesTableView.refreshControl?.endRefreshing()
    }
    
    func setTableEditing(to isEditing: Bool) {
        DispatchQueue.main.async {
            self.notesTableView.setEditing(isEditing, animated: true)
            self.navigationItem.leftBarButtonItem?.title = isEditing ? "Cancel" : "Edit"
            self.navigationItem.leftBarButtonItem?.tintColor = isEditing ? UIColor.red : self.view.tintColor
        }
    }
    
    func deleteRow(at indexPath: IndexPath) {
        notesTableView.deleteRows(at: [indexPath], with: .left)
    }
    
    func dismissHighlight(at indexPath: IndexPath) {
        notesTableView.cellForRow(at: indexPath)?.isSelected = false
        notesTableView.cellForRow(at: indexPath)?.isHighlighted = false
    }
    
    func insertNewNote() {
        notesTableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .right)
    }
    
    func updateOldNote(at indexPath: IndexPath) {
        notesTableView.moveRow(at: indexPath, to: IndexPath(row: 0, section: 0))
        notesTableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .right)
    }
}
