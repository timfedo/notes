//
//  NotesInteructor.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 30/08/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import Foundation
import CoreData

protocol NotesInteructorProtocol {
    var notes: [Note] { get }
    func loadNotes(completion: @escaping () -> Void)
    func addNewNote(_ note: Note)
    func updateNote(_ note: Note, at index: Int)
    func removeNote(at index: Int)
    func setNoAuthHandler(_ handler: @escaping () -> Void)
    func setFailureHandler(_ handler: @escaping () -> Void)
}

class NotesInteructor: NotesInteructorProtocol {

    var notes: [Note] = []
    private var backgroundContext: NSManagedObjectContext!
    private var failureHandler: (() -> Void)!
    
    private let commonQueue = OperationQueue()
    private let dbQueue = OperationQueue()
    private let backendQueue = OperationQueue()
    
    init(backgroundContext: NSManagedObjectContext) {
        self.backgroundContext = backgroundContext
        commonQueue.maxConcurrentOperationCount = 1
        dbQueue.maxConcurrentOperationCount = 1
        backendQueue.maxConcurrentOperationCount = 1
    }
    
    func loadNotes(completion: @escaping () -> Void) {
        let loadOperation = LoadNotesOperation(backgroundContext: backgroundContext,
                                               backendQueue: backendQueue,
                                               dbQueue: dbQueue,
                                               failureHandler: failureHandler)

        let updateTableOperation = BlockOperation {
            self.notes = loadOperation.result
            completion()
        }
        updateTableOperation.addDependency(loadOperation)

        commonQueue.addOperation(loadOperation)
        OperationQueue.main.addOperation(updateTableOperation)
    }
    
    func addNewNote(_ note: Note) {
        addNote(note)
    }
    
    func updateNote(_ note: Note, at index: Int) {
        notes.remove(at: index)
        addNote(note)
    }
    
    func removeNote(at index: Int) {
        let uid = notes[index].uid
        notes.remove(at: index)

        let removeOperation = RemoveNoteOperation(uid: uid,
                                                  notes: notes,
                                                  backgroundContext: backgroundContext,
                                                  backendQueue: backendQueue,
                                                  dbQueue: dbQueue,
                                                  failureHandler: failureHandler)

        commonQueue.addOperation(removeOperation)
    }
    
    func setNoAuthHandler(_ handler: @escaping () -> Void) {
        BaseBackendOperation.noAuthorizationHandler = handler
    }
    
    func setFailureHandler(_ handler: @escaping () -> Void) {
        self.failureHandler = handler
    }
    
    private func addNote(_ note: Note) {
        notes.append(note)
        let saveOperation = SaveNoteOperation(note: note,
                                              notes: notes,
                                              backgroundContext: backgroundContext,
                                              backendQueue: backendQueue,
                                              dbQueue: dbQueue,
                                              failureHandler: failureHandler)

        commonQueue.addOperation(saveOperation)
    }
}
