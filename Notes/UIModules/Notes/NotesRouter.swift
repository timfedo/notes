//
//  NotesViewCoordinator.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 28/08/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import UIKit

protocol NotesRouterProtocol {
    func showNoteEditView(note: Note, navigationTitle: String, saveNote: @escaping (Note) -> Void)
    func showAuthorizationView(completion: @escaping () -> Void)
    func showAlert(title: String, message: String, handler: @escaping () -> Void)
}

class NotesRouter: NotesRouterProtocol {
    
    private weak var notesViewController: UIViewController!
    
    init(notesController: UIViewController) {
        self.notesViewController = notesController
    }
    
    func showNoteEditView(note: Note, navigationTitle: String, saveNote: @escaping (Note) -> Void) {
        let noteEditViewController = NoteEditViewController()
        noteEditViewController.configurate(note: note, title: navigationTitle, saveNote: saveNote)
        notesViewController.navigationController?.pushViewController(noteEditViewController, animated: true)
    }
    
    func showAuthorizationView(completion: @escaping () -> Void) {
        let authorizationViewController = AuthorizationViewController()
        authorizationViewController.configure(completion: completion)
        notesViewController.present(authorizationViewController, animated: true)
    }
    
    func showAlert(title: String, message: String, handler: @escaping () -> Void) {
        self.notesViewController.showAlert(title: title, message: message, handler: handler)
    }
}
