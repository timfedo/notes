//
//  MainCoordinator.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 28/08/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper
import CoreData

// Производит настройку tabbar и navigation view после запуска приложения.
// Также отвечает за показ authorization view при первом запуске приложения
class MainRouter {
    
    private let window: UIWindow?
    private let backgroundContext: NSManagedObjectContext
    private let tabBarController = UITabBarController()
    private let notesNavigationController = UINavigationController()
    private let galleryNavigationController = UINavigationController()
    
    
    init(window: UIWindow?, backgroundContext: NSManagedObjectContext) {
        self.window = window
        self.backgroundContext = backgroundContext
    }
    
    func start() {
        setupTabBarAndNavigationControllers(in: window)
        
        // KeychainWrapper используется для безопасного хранения токена и логина
        Authorization.shared.token = KeychainWrapper.standard.string(forKey: "token") ?? ""
        Authorization.shared.login = KeychainWrapper.standard.string(forKey: "login") ?? ""
        
        if Authorization.shared.token.isEmpty, Authorization.shared.login.isEmpty {
            let authorizationController = AuthorizationViewController()
            authorizationController.configure(completion: setupAndShowNotesAndGallery)
            tabBarController.present(authorizationController, animated: false, completion: nil)
        } else {
            setupAndShowNotesAndGallery()
        }
    }
    
    private func setupTabBarAndNavigationControllers(in window: UIWindow?) {
        guard let window = window else { return }
        window.rootViewController = tabBarController
        notesNavigationController.tabBarItem =
            UITabBarItem(title: "Заметки", image: UIImage(named: "NotesIcon"), selectedImage: nil)
        galleryNavigationController.tabBarItem =
            UITabBarItem(title: "Галерея", image: UIImage(named: "GalleryIcon"), selectedImage: nil)
        tabBarController.viewControllers = [notesNavigationController, galleryNavigationController]
        window.makeKeyAndVisible()
    }
    
    private func setupAndShowNotesAndGallery() {
        let notesViewController = NotesViewController()
        notesViewController.configurate(backgroundContext: backgroundContext)
        notesNavigationController.pushViewController(notesViewController, animated: false)
        let galleryViewController = GalleryViewController()
        galleryViewController.configurate()
        galleryNavigationController.pushViewController(galleryViewController, animated: false)
    }
}
