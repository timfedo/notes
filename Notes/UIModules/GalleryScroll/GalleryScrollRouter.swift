//
//  GalleryScrollRouter.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 31/08/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import UIKit

// Протокол и класс на данном этапе никак не используются, так как возвращение назад
// из просмотра происходит по нажатию дефолтной кнопки navigation controller

// Является просто шаблоном, но инициализируется при работе приложения, также презентер имеет на него ссылку

protocol GalleryScrollRouterProtocol {
    
}

class GalleryScrollRouter: GalleryScrollRouterProtocol {
    
    private weak var galleryScrollViewController: UIViewController!
    
    init(galleryScrollViewController: UIViewController) {
        self.galleryScrollViewController = galleryScrollViewController
    }
}
