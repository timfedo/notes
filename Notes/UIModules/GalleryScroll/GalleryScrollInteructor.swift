//
//  GalleryScrollInteructor.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 31/08/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import UIKit

protocol GalleryScrollInteructorProtocol {
    var images: [UIImage] { get }
}

class GalleryScrollInteructor: GalleryScrollInteructorProtocol {
    
    var images: [UIImage] = []
    
    init(images: [UIImage]) {
        self.images = images
    }
}
