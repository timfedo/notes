//
//  GalleryScrollViewController.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 19/07/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import UIKit

protocol GalleryScrollViewControllerProtocol: class {
    func setNumberOfPages(_ count: Int)
    func setContentOffset(_ offsetPages: Int, animated: Bool)
    func addImage(_ image: UIImage)
    func setupseSizes()
    func setPageControlPage(_ index: Int)
}

class GalleryScrollViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var galleryScrollPresenter: GalleryScrollPresenterProtocol!
    var imageViews: [UIImageView] = []
    
    @IBAction func pageControlChanged(_ sender: UIPageControl) {
        galleryScrollPresenter.pageControlChanged(to: sender.currentPage)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        galleryScrollPresenter.viewLoaded()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        galleryScrollPresenter.subviewsLayouted()
    }
    
    func configurate(startingAt: Int, images: [UIImage]) {
        let galleryScrollInteructor = GalleryScrollInteructor(images: images)
        let galleryScrollRouter = GalleryScrollRouter(galleryScrollViewController: self)
        galleryScrollPresenter = GalleryScrollPresenter(currentIndex: startingAt,
                                                        galleryScrollViewController: self,
                                                        galleryScrollInteructor: galleryScrollInteructor,
                                                        galleryScrollRouter: galleryScrollRouter)
    }
}

extension GalleryScrollViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let index = Int((scrollView.contentOffset.x / scrollView.frame.width).rounded())
        galleryScrollPresenter.startedScrolling(index: index)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        galleryScrollPresenter.endedScrolling(index: pageControl.currentPage)
    }
}

extension GalleryScrollViewController: GalleryScrollViewControllerProtocol {
    
    func setNumberOfPages(_ count: Int) {
        pageControl.numberOfPages = count
    }
    
    func setContentOffset(_ offsetPages: Int, animated: Bool) {
        let offset = scrollView.frame.width * CGFloat(offsetPages)
        scrollView.setContentOffset(CGPoint(x: offset, y: 0), animated: animated)
    }
    
    func addImage(_ image: UIImage) {
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        scrollView.addSubview(imageView)
        imageViews.append(imageView)
    }
    
    func setupseSizes() {
        for (index, imageView) in imageViews.enumerated() {
            imageView.frame = CGRect(x: scrollView.frame.width * CGFloat(index),
                                     y: 0,
                                     width: scrollView.frame.width,
                                     height: scrollView.frame.height)
        }
        scrollView.contentSize = CGSize(width: scrollView.frame.width * CGFloat(imageViews.count),
            height: scrollView.frame.height)
    }
    
    func setPageControlPage(_ index: Int) {
        pageControl.currentPage = index
    }
}
