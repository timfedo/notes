//
//  GalleryScrollViewPresentor.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 25/08/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import Foundation
import UIKit.UIImage

protocol GalleryScrollPresenterProtocol {
    func viewLoaded()
    func subviewsLayouted()
    func startedScrolling(index: Int)
    func endedScrolling(index: Int)
    func pageControlChanged(to index: Int)
}

class GalleryScrollPresenter: GalleryScrollPresenterProtocol {
    
    weak var galleryScrollViewController: GalleryScrollViewControllerProtocol!
    private var galleryScrollInteructor: GalleryScrollInteructorProtocol!
    private var galleryScrollRouter: GalleryScrollRouterProtocol!
    private var currentIndex = 0
    
    init(currentIndex: Int,
         galleryScrollViewController: GalleryScrollViewControllerProtocol,
         galleryScrollInteructor: GalleryScrollInteructorProtocol,
         galleryScrollRouter: GalleryScrollRouterProtocol) {
        self.currentIndex = currentIndex
        self.galleryScrollViewController = galleryScrollViewController
        self.galleryScrollInteructor = galleryScrollInteructor
        self.galleryScrollRouter = galleryScrollRouter
    }
    
    func viewLoaded() {
        setupImages()
    }
    
    func subviewsLayouted() {
        galleryScrollViewController.setContentOffset(currentIndex, animated: false)
        galleryScrollViewController.setupseSizes()
    }
    
    func startedScrolling(index: Int) {
        galleryScrollViewController.setPageControlPage(index)
    }
    
    func endedScrolling(index: Int) {
        currentIndex = index
    }
    
    func pageControlChanged(to index: Int) {
        let prevIndex = currentIndex
        currentIndex = index
        galleryScrollViewController.setPageControlPage(prevIndex)
        galleryScrollViewController.setContentOffset(currentIndex, animated: true)
    }
    
    private func setupImages() {
        galleryScrollViewController.setNumberOfPages(galleryScrollInteructor.images.count)
        galleryScrollViewController.setPageControlPage(currentIndex)
        for image in galleryScrollInteructor.images {
            galleryScrollViewController.addImage(image)
        }
    }
}
