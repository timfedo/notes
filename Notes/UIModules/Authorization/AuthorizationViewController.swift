//
//  AuthorizationViewController.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 11/08/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import UIKit
import WebKit
import SwiftKeychainWrapper

protocol AuthorizationViewControllerProtocol: class {
    func loadPage(with request: URLRequest)
}

class AuthorizationViewController: UIViewController {
    
    @IBOutlet weak var webView: WKWebView!
    private var authorizationPresenter: AuthorizationPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.navigationDelegate = self
        authorizationPresenter.viewLoaded()
    }
    
    func configure(completion: @escaping () -> Void) {
        let authorizationRouter = AuthorizationRouter(authorizationController: self)
        let authorizationInteructor = AuthorizationInteructor()
        self.authorizationPresenter = AuthorizationPresenter(authorizationViewController: self,
                                                             authorizationInteructor: authorizationInteructor,
                                                             authorizationRouter: authorizationRouter,
                                                             completion: completion)
    }
}

extension AuthorizationViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        authorizationPresenter.failedNavigation(with: error)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        defer {
            decisionHandler(.allow)
        }
        authorizationPresenter.loadAuthorizationData(redirectUrl: navigationAction.request.url)
    }
}

extension AuthorizationViewController: AuthorizationViewControllerProtocol {
    func loadPage(with request: URLRequest) {
        self.webView.load(request)
    }
}
