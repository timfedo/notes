//
//  AuthorizationInteructor.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 30/08/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

struct Token: Decodable {
    let access_token: String
}

struct User: Decodable {
    let login: String
}

protocol AuthorizationInteructorProtocol {
    func buildRequest() -> URLRequest?
    func loadAuthorizationData(redirectUrl: URL?, completion: @escaping () -> Void)
}

class AuthorizationInteructor: AuthorizationInteructorProtocol {
    
    func buildRequest() -> URLRequest? {
        guard var urlComponents = URLComponents(string: "https://github.com/login/oauth/authorize") else {
            return nil
        }
        urlComponents.queryItems = [
            URLQueryItem(name: "client_id", value: Authorization.clientId),
            URLQueryItem(name: "scope", value: "gist")
        ]
        guard let url = urlComponents.url else {
            return nil
        }
        return URLRequest(url: url)
    }
    
    func loadAuthorizationData(redirectUrl: URL?, completion: @escaping () -> Void) {
        if let url = redirectUrl {
            let targetString = url.absoluteString.replacingOccurrences(of: "#", with: "?")
            guard let components = URLComponents(string: targetString) else { return }
            if let code = components.queryItems?.first(where: { $0.name == "code"})?.value {
                guard var urlComponents =
                    URLComponents(string: "https://github.com/login/oauth/access_token") else { return }
                urlComponents.queryItems = [
                    URLQueryItem(name: "client_id", value: Authorization.clientId),
                    URLQueryItem(name: "client_secret", value: Authorization.clientSecret),
                    URLQueryItem(name: "code", value: "\(code)")
                ]
                guard let url = urlComponents.url else { return }
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Accept")
                let task = URLSession.shared.dataTask(with: request) { [weak self] data, respomse, error in
                    guard let _ = self, let data = data else { return }
                    guard let token = try? JSONDecoder().decode(Token.self, from: data) else { return }
                    KeychainWrapper.standard.set(token.access_token, forKey: "token")
                    Authorization.shared.token = token.access_token
                    guard let url = URL(string: "https://api.github.com/user") else { return }
                    var request = URLRequest(url: url)
                    request.addValue("token \(token.access_token)", forHTTPHeaderField: "Authorization")
                    URLSession.shared.dataTask(with: request) { data, respomse, error in
                        guard let data = data else { return }
                        guard let user = try? JSONDecoder().decode(User.self, from: data) else { return }
                        KeychainWrapper.standard.set(user.login, forKey: "login")
                        Authorization.shared.login = user.login
                    }.resume()
                    completion()
                }
                task.resume()
            }
        }
    }
}
