//
//  AuthorizationViewPresentor.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 20/08/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import Foundation

protocol AuthorizationPresenterProtocol {
    func failedNavigation(with error: Error)
    func loadAuthorizationData(redirectUrl: URL?)
    func viewLoaded()
}

class AuthorizationPresenter: AuthorizationPresenterProtocol {
    
    private weak var authorizationViewController: AuthorizationViewControllerProtocol!
    private var authorizationRouter: AuthorizationRouterProtocol!
    private var authorizationInteructor: AuthorizationInteructorProtocol!
    private var completion: (() -> Void)!
    
    init(authorizationViewController: AuthorizationViewControllerProtocol,
         authorizationInteructor: AuthorizationInteructorProtocol,
         authorizationRouter: AuthorizationRouterProtocol,
         completion: @escaping () -> Void) {
        self.authorizationViewController = authorizationViewController
        self.authorizationInteructor = authorizationInteructor
        self.authorizationRouter = authorizationRouter
        self.completion = completion
    }
    
    func failedNavigation(with error: Error) {
        guard let urlError = error as? URLError else { return }
        if urlError.code == .notConnectedToInternet {
            DispatchQueue.main.async {
                self.authorizationRouter.showAlert(title: "Нет интернет соединения",
                                              message: "Вам будет доступно редактирование заметок оффлайн, но при следующем подключении к сети заметки будут перезаписаны заметками с сервера") {
                                                self.authorizationRouter.dismiss()
                }
            }
        }
    }
    
    func loadAuthorizationData(redirectUrl: URL?) {
        authorizationInteructor.loadAuthorizationData(redirectUrl: redirectUrl) {
            self.completion()
            self.authorizationRouter.dismiss()
        }
    }
    
    func viewLoaded() {
        guard let request = authorizationInteructor.buildRequest() else { return }
        authorizationViewController.loadPage(with: request)
    }
}
