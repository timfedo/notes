//
//  AuthorizationViewCoordinator.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 28/08/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import UIKit

protocol AuthorizationRouterProtocol {
    func showAlert(title: String, message: String, handler: @escaping () -> Void)
    func dismiss()
}

class AuthorizationRouter: AuthorizationRouterProtocol {

    private weak var authorizationController: UIViewController!
    
    init(authorizationController: UIViewController) {
        self.authorizationController = authorizationController
    }
    
    func showAlert(title: String, message: String, handler: @escaping () -> Void) {
        self.authorizationController.showAlert(title: title, message: message, handler: handler)
    }
    
    func dismiss() {
        authorizationController.dismiss(animated: true, completion: nil)
    }
}
