//
//  GalleryViewCoordinator.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 29/08/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import UIKit

protocol GalleryRouterProtocol: class {
    func showGalleryScrollView(startingAt: Int, with images: [UIImage])
    func showActionSheet(imagePickerController: UIImagePickerController)
}

class GalleryRouter: GalleryRouterProtocol {
    
    private weak var galleryViewController: UIViewController!
    
    init(galleryViewController: UIViewController) {
        self.galleryViewController = galleryViewController
    }
    
    func showGalleryScrollView(startingAt: Int, with images: [UIImage]) {
        let galleryScrollViewController = GalleryScrollViewController()
        galleryScrollViewController.configurate(startingAt: startingAt, images: images)
        galleryViewController.navigationController?.pushViewController(galleryScrollViewController, animated: true)
    }
    
    func showActionSheet(imagePickerController: UIImagePickerController) {
        
        let actionSheet = UIAlertController(title: "Выберите источник фото", message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Камера", style: .default, handler: { (_ :UIAlertAction) in
            guard UIImagePickerController.isSourceTypeAvailable(.camera) else { return }
            imagePickerController.sourceType = .camera
            self.galleryViewController.present(imagePickerController, animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Галерея", style: .default, handler: { (_ :UIAlertAction) in
            imagePickerController.sourceType = .photoLibrary
            self.galleryViewController.present(imagePickerController, animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        self.galleryViewController.present(actionSheet, animated: true, completion: nil)
    }
}
