//
//  GalleryViewController.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 18/07/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import UIKit

protocol GalleryViewControllerProtocol: class {
    func addItemToColletionView()
    func updateCollectionView()
    func getImagePickerController() -> UIImagePickerController
}

class GalleryViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    private var reuseIdentifier = "gallery cell"
    private var galleryPresenter: GalleryPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Галерея"
        navigationItem.rightBarButtonItem =
            UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addButtonAction))
        
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        collectionView.refreshControl = UIRefreshControl()
        collectionView.refreshControl?.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
        collectionView.refreshControl?.beginRefreshing()
        galleryPresenter.viewLoaded()
    }
    
    func configurate() {
        let galleryInteructor = GalleryInteructor()
        let galleryRouter = GalleryRouter(galleryViewController: self)
        galleryPresenter = GalleryPresenter(galleryController: self,
                                            galleryInteructor: galleryInteructor,
                                            galleryRouter: galleryRouter)
    }
    
    @objc private func addButtonAction() {
        galleryPresenter.addTapped()
    }
    
    @objc private func onRefresh() {
        galleryPresenter.updateData()
    }
}

extension GalleryViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return galleryPresenter.getImagesCount()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        let imageView = UIImageView(image: galleryPresenter.getImage(at: indexPath))
        imageView.contentMode = .scaleAspectFill
        imageView.layer.masksToBounds = true
        imageView.frame = cell.bounds
        imageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        cell.addSubview(imageView)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let availableWidth = collectionView.frame.width - CGFloat(16 * 4)
        let widthPerItem = floor(availableWidth / 3)
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        galleryPresenter.selectedImage(at: indexPath)
    }
}

extension GalleryViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        galleryPresenter.imageAdded(image)
        dismiss(animated: true, completion: nil)
    }
}

extension GalleryViewController: GalleryViewControllerProtocol {
    
    func addItemToColletionView() {
        collectionView.insertItems(at: [IndexPath(row: collectionView.numberOfItems(inSection: 0), section: 0)])
    }
    
    func updateCollectionView() {
        collectionView.reloadData()
        collectionView.refreshControl?.endRefreshing()
    }
    
    func getImagePickerController() -> UIImagePickerController {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        return imagePickerController
    }
}
