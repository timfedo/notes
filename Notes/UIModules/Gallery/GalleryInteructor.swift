//
//  GalleryInteructor.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 31/08/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import UIKit

protocol GalleryInteructorProtocol {
    var images: [UIImage] { get }
    func addImageToGallery(_ image: UIImage)
    func loadImages(completion: @escaping () -> Void)
}

class GalleryInteructor: GalleryInteructorProtocol {
    
    var images: [UIImage] {
        get {
            return gallery.images
        }
    }
    let gallery = FileGallery()
    
    func addImageToGallery(_ image: UIImage) {
        DispatchQueue.global(qos: .userInitiated).async {
            self.gallery.add(image)
            self.gallery.saveToFile()
        }
    }
    
    func loadImages(completion: @escaping () -> Void) {
        DispatchQueue.global(qos: .userInitiated).async {
            self.gallery.loadFromFile()
            DispatchQueue.main.async {
                completion()
            }
        }
    }
}
