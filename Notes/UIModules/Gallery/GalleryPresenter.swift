//
//  GalleryViewPresentor.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 25/08/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import Foundation
import UIKit.UIImage

protocol GalleryPresenterProtocol {
    func addTapped()
    func getImagesCount() -> Int
    func getImage(at indexPath: IndexPath) -> UIImage
    func selectedImage(at indexPath: IndexPath)
    func imageAdded(_ image: UIImage)
    func viewLoaded()
    func updateData()
}

class GalleryPresenter: GalleryPresenterProtocol {
    
    private weak var galleryController: GalleryViewControllerProtocol!
    private var galleryInteructor: GalleryInteructorProtocol!
    private var galleryRouter: GalleryRouterProtocol!
    
    init(galleryController: GalleryViewControllerProtocol,
         galleryInteructor: GalleryInteructorProtocol,
         galleryRouter: GalleryRouterProtocol) {
        self.galleryController = galleryController
        self.galleryInteructor = galleryInteructor
        self.galleryRouter = galleryRouter
    }
    
    func addTapped() {
        galleryRouter.showActionSheet(imagePickerController: galleryController.getImagePickerController())
    }
    
    func getImagesCount() -> Int {
        return galleryInteructor.images.count
    }
    
    func getImage(at indexPath: IndexPath) -> UIImage {
        return galleryInteructor.images[indexPath.row]
    }
    
    func selectedImage(at indexPath: IndexPath) {
        galleryRouter.showGalleryScrollView(startingAt: indexPath.row, with: galleryInteructor.images)
    }
    
    func imageAdded(_ image: UIImage) {
        galleryInteructor.addImageToGallery(image)
        galleryController.addItemToColletionView()
    }
    
    func viewLoaded() {
        updateData()
    }
    
    func updateData() {
        galleryInteructor.loadImages {
            self.galleryController.updateCollectionView()
        }
    }
}
