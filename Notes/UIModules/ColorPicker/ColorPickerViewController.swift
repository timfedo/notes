//
//  ColorPickerController.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 10/07/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import UIKit

protocol ColorPickerViewControllerProtocol: class {
    func setColorToPicker(_ color: [CGFloat])
}

class ColorPickerViewController: UIViewController,  UIGestureRecognizerDelegate {

    var colorPickerPresenter: ColorPickerPresenterProtocol!
    
    @IBOutlet weak var colorPickerView: ColorPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Выбор цвета"
        navigationItem.leftBarButtonItem =
            UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelButtonAction))
        navigationItem.leftBarButtonItem?.tintColor = .red
        navigationItem.rightBarButtonItem =
            UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonAction))
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        colorPickerPresenter.viewLoaded()
    }
    
    func configurate(color: [CGFloat], saveColor: @escaping ([CGFloat]) -> Void) {
        let colorPickerInteructor = ColorPickerInteructor(color: color)
        let colorPickerRouter = ColorPickerRouter(colorPickerController: self)
        colorPickerPresenter = ColorPickerPresenter(colorPickerViewController: self,
                                                    colorPickerInteructor: colorPickerInteructor,
                                                    colorPickerRouter: colorPickerRouter,
                                                    saveColor: saveColor)
    }
    
    @objc private func cancelButtonAction() {
        colorPickerPresenter.cancelTapped()
    }
    
    @objc private func doneButtonAction() {
        colorPickerPresenter.setColorFromPicker(colorPickerView.color)
        colorPickerPresenter.doneTapped()
    }
}

extension ColorPickerViewController: ColorPickerViewControllerProtocol {
    func setColorToPicker(_ color: [CGFloat]) {
        colorPickerView.color = color
    }
}
