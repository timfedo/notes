//
//  ColorPickerInteructor.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 31/08/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import UIKit

protocol ColorPickerInteructorProtocol {
    var color: [CGFloat] { get set }
}

class ColorPickerInteructor: ColorPickerInteructorProtocol {
    
    var color: [CGFloat]
    
    init(color: [CGFloat]) {
        self.color = color
    }
}
