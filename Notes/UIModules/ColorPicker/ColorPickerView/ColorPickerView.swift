//
//  ColorPickerView.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 10/07/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import UIKit

@IBDesignable
class ColorPickerView : UIView {
        
    private let paletteView = PaletteView()
    private let crosshairView = UIView()
    private let crosshairSize: CGFloat = 24
    
    @IBOutlet weak var currentColor: UIView!
    @IBOutlet weak var currentColorHex: UILabel!
    @IBOutlet weak var paletteHolderView: UIView!
    @IBOutlet weak var brightnessPicker: UISlider!
    
    var color: [CGFloat]!
    
    private var holderHeight: CGFloat = 0
    private var holderWidth: CGFloat = 0
    
    private var uiColor: UIColor {
        get {
            return UIColor(hue: color[0],
                           saturation: 1 - color[1],
                           brightness: color[2],
                           alpha: 1)
        }
    }
    
    private var colorHexString: String {
        get {
            var r: CGFloat = 0
            var g: CGFloat = 0
            var b: CGFloat = 0
            var a: CGFloat = 0
            uiColor.getRed(&r, green: &g, blue: &b, alpha: &a)
            return "#" + String(format: "%.2X", Int(round(r*255))) + String(format: "%.2X", Int(round(g*255)))
                + String(format: "%.2X", Int(round(b*255)))
        }
    }
    
    @IBAction func onBrightnessChanged(_ sender: UISlider) {
        color[2] = CGFloat(sender.value)
        updateUI()
    }
    
    @IBAction func onPalettePanned(_ sender: UIPanGestureRecognizer) {
        if sender.state == .began || sender.state == .changed {
            onPaletteTouched(sender)
        }
    }
    
    @IBAction func onPaletteTapped(_ sender: UITapGestureRecognizer) {
        onPaletteTouched(sender)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }
    
    override func layoutSubviews() {
        // Вычисляются размеры paletteHolderView для последующей отрисовки прицела.
        super.layoutSubviews()
        holderWidth = self.bounds.width - 16*2
        holderHeight = self.bounds.height - 8 - 16*2 - currentColor.frame.height
        updateUI()
    }
    
    
    private func onPaletteTouched(_ sender: UIGestureRecognizer) {
        let touchPoint = sender.location(in: paletteHolderView)
        color[0] = checkColorInBounds(touchPoint.x / holderWidth)
        color[1] = checkColorInBounds(touchPoint.y / holderHeight)
        updateUI()
    }

    private func checkColorInBounds(_ color: CGFloat) -> CGFloat {
        // Здесь проверяется что полученное аргументом значение в правильных пределах [0; 1].
        // Если значение меньше допустимого возвращается 0, а если больше, то 1.
        // Таким образом при перетаскивании курсора палец может выходить за пределы paletteHolderView,
        // а сам курсор будет ограничен рамкой + цвет в hex строке тоже не будет выходить за пределы
        if color < 0 {
            return 0
        }
        if color > 1 {
            return 1
        }
        return color
    }
    
    private func setupViews() {
        let xibView = loadViewFromXib()
        xibView.frame = self.bounds
        xibView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.addSubview(xibView)
        
        paletteView.frame = paletteHolderView.bounds
        paletteView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        paletteHolderView.addSubview(paletteView)
        crosshairView.isOpaque = false
        paletteHolderView.addSubview(crosshairView)
        paletteHolderView.clipsToBounds = true
        setupObjectsInView()
    }
    
    private func setupObjectsInView() {
        currentColor.layer.borderColor = UIColor.black.cgColor
        currentColor.layer.borderWidth = 2
        currentColor.layer.cornerRadius = 5
        paletteHolderView.layer.borderColor = UIColor.black.cgColor
        paletteHolderView.layer.borderWidth = 2
        drawLineInCurrentColor()
        drawCrosshair()
    }
    
    private func loadViewFromXib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "ColorPickerView", bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first! as! UIView
    }
    
    private func drawLineInCurrentColor() {
        // Отрисовка линии между превью цвета и строкой hex
        let shapeLayer = CAShapeLayer()
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: currentColorHex.frame.width, y:0))
        shapeLayer.path = path.cgPath
        shapeLayer.strokeColor = UIColor.black.cgColor
        shapeLayer.lineWidth = 2
        shapeLayer.position = CGPoint(x: currentColorHex.frame.minX, y: currentColorHex.frame.minY)
        currentColor.layer.addSublayer(shapeLayer)
    }
    
    func updateUI() {
        brightnessPicker.value = Float(color[2])
        currentColor.backgroundColor = uiColor
        currentColorHex.text = colorHexString
        paletteView.layer.opacity = Float(color[2])
        crosshairView.frame = CGRect(x: color[0]*holderWidth - crosshairSize/2,
                                     y: color[1]*holderHeight - crosshairSize/2,
                                     width: crosshairSize,
                                     height: crosshairSize)
        if color[2] < 0.5 {
            (crosshairView.layer.sublayers?.first as? CAShapeLayer)?.strokeColor = UIColor.white.cgColor
        } else {
            (crosshairView.layer.sublayers?.first as? CAShapeLayer)?.strokeColor = UIColor.black.cgColor
        }
    }
    
    private func drawCrosshair() {
        // Отрисовка прицела
        let shapeLayer = CAShapeLayer()
        let path = UIBezierPath()
        path.addArc(withCenter: CGPoint(x: 12, y: 12), radius: 7,
                    startAngle: 0, endAngle: CGFloat(2*Double.pi), clockwise: true)
        path.move(to: CGPoint(x: 12, y: 5))
        path.addLine(to: CGPoint(x: 12, y: 0))
        path.move(to: CGPoint(x: 12, y: 19))
        path.addLine(to: CGPoint(x: 12, y: 24))
        path.move(to: CGPoint(x: 5, y: 12))
        path.addLine(to: CGPoint(x: 0, y: 12))
        path.move(to: CGPoint(x: 19, y: 12))
        path.addLine(to: CGPoint(x: 24, y: 12))
        shapeLayer.path = path.cgPath
        shapeLayer.strokeColor = UIColor.black.cgColor
        shapeLayer.lineWidth = 2
        shapeLayer.fillColor = UIColor(white: 1, alpha: 0).cgColor
        shapeLayer.position = CGPoint(x: crosshairView.bounds.minX, y: crosshairView.bounds.minY)
        crosshairView.layer.addSublayer(shapeLayer)
    }
}
