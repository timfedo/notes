//
//  ColorHolder.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 08/07/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import UIKit

@IBDesignable
class ColorView: UIView {
    
    // Кастомный вью для отображения квадратиков с цетом. Свойство isDarkColor чтобы определить
    // какого цвета должен быть флажок
    
    var isDarkColor = false
    
    @IBInspectable var color: UIColor? {
        get {
            return self.backgroundColor
        }
        set {
            for view in self.subviews {
                view.removeFromSuperview()
            }
            self.backgroundColor = newValue
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable var isSelected: Bool = false {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    private var bgImage: UIImageView?
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.borderWidth = 2
        if isSelected {
            createCheckmarkPath()
        }
        if color == nil {
            setBgImage()
        }
    }

    private func setBgImage() {
        // Установка картинки палитры
        let dynamicBundle = Bundle(for: type(of: self))
        let palette = UIImage(named: "PaletteMiniature", in: dynamicBundle, compatibleWith: nil)!
        bgImage = UIImageView(image: palette)
        bgImage?.frame = self.bounds
        bgImage?.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        if let bgImage = bgImage {
            self.addSubview(bgImage)
        }
    }
    
    private func createCheckmarkPath() {
        // Отрисовка галочки
        let path = UIBezierPath()
        path.addArc(withCenter: CGPoint(x: self.bounds.width - 20, y: 20), radius: 13, startAngle: 0,
                    endAngle: CGFloat(2*Double.pi), clockwise: false)
        path.move(to: CGPoint(x: self.bounds.width - 25, y: 20))
        path.addLine(to: CGPoint(x: self.bounds.width - 20, y: 25))
        path.addLine(to: CGPoint(x: self.bounds.width - 15, y: 15))
        path.lineWidth = 4
        if isDarkColor {
            UIColor.white.setStroke()
        } else {
            UIColor.black.setStroke()
        }
        path.stroke()
    }
}
