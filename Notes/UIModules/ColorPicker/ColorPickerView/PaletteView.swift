//
//  PaletteView.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 09/07/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import UIKit

@IBDesignable
class PaletteView : UIView {
        
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        drawPalette()
    }
    
    private func drawPalette() {
        let viewWidth = self.bounds.width
        let viewHeight = self.bounds.height
        let width: CGFloat = 2
        let height: CGFloat = 2
        // Палитра рисуется маленькими квадратиками. Цвет каждого вычисляется на основе его положения внутри вью
        // Здесь нигде не меняется brightness, это сделано потому, что палитра рисуется довольно долго
        // и если её рисовать каждый раз когда меняется положение слайдера появятся сильные тормоза.
        // Вместо этого у супервью этого вью черный фон, а при изменении положения слайдера меняется прозрачность
        // всего PaletteView
        for x in stride(from: 0, to: viewWidth, by: width) {
            let hue = x/viewWidth
            for y in stride(from: 0, to: viewHeight, by: height) {
                UIColor(hue: hue, saturation: 1 - y/viewHeight, brightness: 1, alpha: 1).setFill()
                UIBezierPath(rect: CGRect(x: x, y: y, width: width, height: height)).fill()
            }
        }
    }
}
