//
//  ColorPickerRouter.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 31/08/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import UIKit

protocol ColorPickerRouterProtocol {
    func pop()
}

class ColorPickerRouter: ColorPickerRouterProtocol {
    
    private weak var colorPickerController: UIViewController!
    
    init(colorPickerController: UIViewController) {
        self.colorPickerController = colorPickerController
    }
    
    func pop() {
        colorPickerController.navigationController?.popViewController(animated: true)
    }
}
