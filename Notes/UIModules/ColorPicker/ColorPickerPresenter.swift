//
//  ColorPickerViewPresentor.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 24/08/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import UIKit

protocol ColorPickerPresenterProtocol {
    func cancelTapped()
    func doneTapped()
    func viewLoaded()
    func setColorFromPicker(_ color: [CGFloat])
}

class ColorPickerPresenter: ColorPickerPresenterProtocol {
    
    private weak var colorPickerViewController: ColorPickerViewControllerProtocol!
    private var colorPickerInteructor: ColorPickerInteructorProtocol!
    private var colorPickerRouter: ColorPickerRouterProtocol!
    private var saveColor: (([CGFloat]) -> Void)!
    
    init(colorPickerViewController: ColorPickerViewControllerProtocol,
         colorPickerInteructor: ColorPickerInteructorProtocol,
         colorPickerRouter: ColorPickerRouterProtocol,
         saveColor: @escaping ([CGFloat]) -> Void) {
        self.colorPickerViewController = colorPickerViewController
        self.colorPickerInteructor = colorPickerInteructor
        self.colorPickerRouter = colorPickerRouter
        self.saveColor = saveColor
    }
    
    func cancelTapped() {
        colorPickerRouter.pop()
    }
    
    func doneTapped() {
        saveColor(colorPickerInteructor.color)
        colorPickerRouter.pop()
    }
    
    func viewLoaded() {
        colorPickerViewController.setColorToPicker(colorPickerInteructor.color)
    }
    
    func setColorFromPicker(_ color: [CGFloat]) {
        colorPickerInteructor.color = color
    }
}
