//
//  RemoveNoteOperation.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 24/07/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import Foundation
import CoreData

class RemoveNoteOperation: AsyncOperation {
    private let removeFromDb: RemoveNoteDBOperation
    private let dbQueue: OperationQueue
    
    private(set) var result: Bool? = false
    
    // failureHandler вызывается при неудаче загрузки из интернета
    init(uid: String,
         notes: [Note],
         backgroundContext: NSManagedObjectContext,
         backendQueue: OperationQueue,
         dbQueue: OperationQueue,
         failureHandler: @escaping () -> Void) {
        
        removeFromDb = RemoveNoteDBOperation(with: uid, backgroundContext: backgroundContext)
        self.dbQueue = dbQueue
        
        super.init()
        
        removeFromDb.completionBlock = {
            let saveToBackend = SaveNotesBackendOperation(notes: notes)
            saveToBackend.completionBlock = {
                switch saveToBackend.result! {
                case .success:
                    self.result = true
                    break
                case .failure:
                    failureHandler()
                    self.result = false
                    break
                }
                self.finish()
            }
            backendQueue.addOperation(saveToBackend)
        }
    }
    
    override func main() {
        dbQueue.addOperation(removeFromDb)
    }
}

