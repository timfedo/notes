//
//  SaveNotesBackendOperation.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 24/07/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import Foundation

enum SaveNotesBackendResult {
    case success
    case failure(NetworkError)
}

struct UploadGist: Codable {
    let files: [String: UploadGistFile]
    let description: String?
    
    init(fileName: String, content: String, description: String) {
        self.files = [fileName: UploadGistFile(content: content)]
        self.description = description
    }
}

struct UploadGistFile: Codable {
    let content: String
}

class SaveNotesBackendOperation: BaseBackendOperation {
    
    var result: SaveNotesBackendResult?
    var notes: [Note]
    
    init(notes: [Note]) {
        self.notes = notes
        super.init()
    }
    
    func createData(notes: [Note]) -> Data? {
        guard let data = try? JSONSerialization.data(withJSONObject: notes.map {$0.json}, options: []) else {
            return nil
        }
        let jsonString = String(data: data, encoding: .utf8)
        let gist = UploadGist(fileName: "ios-course-notes-db", content: jsonString!, description: "notes from note app")
        return try? JSONEncoder().encode(gist)
    }
    
    override func main() {
        var string = "https://api.github.com/gists"
        if !Authorization.shared.gistId.isEmpty {
            string += "/\(Authorization.shared.gistId)"
        }
        guard let url = URL(string: string) else {
            result = .failure(.unreachable)
            finish()
            return
        }
        guard let data = createData(notes: notes) else {
            result = .failure(.dataCorrupted)
            finish()
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = Authorization.shared.gistId.isEmpty ? "POST":"PATCH"
        request.addValue("token \(Authorization.shared.token)", forHTTPHeaderField: "Authorization")
        request.httpBody = data
        let task = URLSession.shared.dataTask(with: request) { [weak self] data, response, error in
            guard self != nil else {
                self?.finish()
                return
            }
            guard let self = self else { return }
            guard error == nil else {
                self.result = .failure(.unreachable)
                self.finish()
                return
            }
            if let response = response as? HTTPURLResponse {
                switch response.statusCode {
                case 200..<300:
                    self.result = .success
                    guard let data = data else {
                        self.finish()
                        return
                    }
                    guard let gist = try? JSONDecoder().decode(RecievedGist.self, from: data) else {
                        self.finish()
                        return
                    }
                    Authorization.shared.gistId = gist.id
                case 401:
                    self.result = .failure(.noAuth)
                    BaseBackendOperation.noAuthorizationHandler()
                default:
                    self.result = .failure(.defaultError)
                }
            }
            self.finish()
        }
        task.resume()
    }
}
