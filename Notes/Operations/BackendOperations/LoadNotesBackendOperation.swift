//
//  LoadNotesBackendOperation.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 24/07/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import Foundation

enum LoadNotesBackendResult {
    case success([Note])
    case failure(NetworkError)
}

struct RecievedGist: Codable {
    let id: String
    let files: [String: RecievedGistFile]
}

struct RecievedGistFile: Codable {
    let raw_url: String
}

class LoadNotesBackendOperation: BaseBackendOperation {
    
    var result: LoadNotesBackendResult?
    
    override func main() {
        let string = "https://api.github.com/users/\(Authorization.shared.login)/gists"
        guard let url = URL(string: string) else { return }
        var request = URLRequest(url: url)
        request.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        request.addValue("token \(Authorization.shared.token)", forHTTPHeaderField: "Authorization")
        let task = URLSession.shared.dataTask(with: request) { [weak self] data, response, error in
            guard self != nil else {
                self?.finish()
                return
            }
            guard let self = self else { return }
            guard error == nil else {
                self.result = .failure(.unreachable)
                self.finish()
                return
            }
            if let response = response as? HTTPURLResponse {
                switch response.statusCode {
                case 200..<300:
                    guard let data = data else {
                        self.result = .failure(.dataCorrupted)
                        self.finish()
                        return
                    }
                    guard let gists = try? JSONDecoder().decode([RecievedGist].self, from: data) else {
                        self.result = .failure(.dataCorrupted)
                        self.finish()
                        return
                    }
                    guard let notesGist =
                        gists.first(where: {$0.files["ios-course-notes-db"] != nil}) else {
                        self.result = .failure(.notFound)
                        self.finish()
                        return
                    }
                    
                    Authorization.shared.gistId = notesGist.id
                    let string = notesGist.files["ios-course-notes-db"]!.raw_url
                    
                    URLSession.shared.dataTask(with: URL(string: string)!) { [weak self] data, response, error in
                        guard error == nil else {
                            self?.result = .failure(.unreachable)
                            self?.finish()
                            return
                        }
                        guard let data = data else {
                            self?.result = .failure(.dataCorrupted)
                            self?.finish()
                            return
                        }
                        if let notesJsonObject = try? JSONSerialization.jsonObject(with: data, options: []) {
                            if let notesJson = notesJsonObject as? Array<Dictionary<String, Any>> {
                                let notes = notesJson.compactMap {Note.parse(json: $0)}
                                // Проверка на уникальность всех uid в полученных данных
                                // Так как множество может содержать только уникальные значения,
                                // количество uid должно быть равно количеству элементов в множестве из uid
                                let uids = notes.map {$0.uid}
                                if uids.count == Set(uids).count {
                                    self?.result = .success(notes)
                                } else {
                                    self?.result = .failure(.dataCorrupted)
                                }
                            } else {
                                self?.result = .failure(.dataCorrupted)
                            }
                        } else {
                            self?.result = .failure(.dataCorrupted)
                        }
                        self?.finish()
                    }.resume()
                case 401:
                    self.result = .failure(.noAuth)
                    BaseBackendOperation.noAuthorizationHandler()
                    self.finish()
                case 404:
                    self.result = .failure(.notFound)
                    self.finish()
                default:
                    self.result = .failure(.defaultError)
                    self.finish()
                }
            }
        }
        task.resume()
    }
}
