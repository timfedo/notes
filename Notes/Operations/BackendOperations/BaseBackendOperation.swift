//
//  BaseBackendOpertation.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 24/07/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import Foundation

enum NetworkError {
    case unreachable
    case noAuth
    case notFound
    case defaultError
    case dataCorrupted
}

struct gistId: Decodable {
    let id: String
}

class BaseBackendOperation: AsyncOperation {
    
    // Этот метод вызывается в бэкэнд операциях при ответе сервера 401 Unauthorized
    static var noAuthorizationHandler: () -> Void = {}

    override init() {
        super.init()
    }
}
