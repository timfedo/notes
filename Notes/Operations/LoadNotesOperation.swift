//
//  LoadNotesOperation.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 24/07/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import Foundation
import CoreData

class LoadNotesOperation: AsyncOperation {
    
    private let loadFromDb: LoadNotesDBOperation
    private let loadFromBackend: LoadNotesBackendOperation
    private let dbQueue: OperationQueue
    private let backgroundContext: NSManagedObjectContext
    private let failureHandler: () -> Void
    
    private(set) var result: [Note] = []
    
    // failureHandler вызывается при неудаче загрузки из интернета
    init(backgroundContext: NSManagedObjectContext,
         backendQueue: OperationQueue,
         dbQueue: OperationQueue,
         failureHandler: @escaping () -> Void) {
        self.dbQueue = dbQueue
        self.backgroundContext = backgroundContext
        self.failureHandler = failureHandler
        
        loadFromDb = LoadNotesDBOperation(backgroundContext: backgroundContext)
        loadFromBackend = LoadNotesBackendOperation()
        loadFromBackend.addDependency(loadFromDb)
        
        super.init()
        
        addDependency(loadFromDb)
        dbQueue.addOperation(loadFromDb)
        addDependency(loadFromBackend)
        backendQueue.addOperation(loadFromBackend)
    }
    
    override func main() {
        switch loadFromBackend.result! {
        case .failure(let type):
            if type != .notFound {
                failureHandler()
            }
            result = loadFromDb.result!
            break
        case .success(let notes):
            result = notes
            dbQueue.addOperation {
                let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "NoteObj")
                let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
                
                // Перезапись заметок на устройстве заметками, полученными с сервера
                self.backgroundContext.performAndWait {
                    do {
                        try self.backgroundContext.execute(deleteRequest)
                        try self.backgroundContext.save()
                    } catch {
                        print ("There was an error")
                    }
                }
            }
            for note in notes {
                let saveToDb = SaveNoteDBOperation(note: note, backgroundContext: backgroundContext)
                dbQueue.addOperation(saveToDb)
            }
            break
        }
        finish()
    }
}
