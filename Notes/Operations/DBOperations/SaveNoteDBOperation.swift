//
//  SaveNoteDBOperation.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 24/07/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import UIKit
import CoreData

class SaveNoteDBOperation: BaseDBOperation {
    
    private let note: Note
    
    init(note: Note, backgroundContext: NSManagedObjectContext) {
        self.note = note
        super.init(backgroundContext: backgroundContext)
    }
    
    override func main() {
        backgroundContext.performAndWait {
            do {
                createNoteObj(note: note)
                try backgroundContext.save()
                finish()
            } catch {
                finish()
            }
        }
    }
    
    private func createNoteObj(note: Note) {
        let color: Color?
        if note.color != UIColor.white {
            color = Color(context: backgroundContext)
            let colors = note.colors
            color!.r = colors[0]
            color!.g = colors[1]
            color!.b = colors[2]
            color!.a = colors[3]
        } else {
            color = nil
        }
        
        let noteObj = NoteObj(context: backgroundContext)
        noteObj.uid = note.uid
        noteObj.title = note.title
        noteObj.content = note.content
        noteObj.color = color
        noteObj.importance = note.importance.rawValue
        noteObj.selfDestructDate = note.selfDestructDate
        // Строка ниже для второй версии модели
        noteObj.editDate = Date()
    }
}
