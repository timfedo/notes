//
//  BaseDBOperation.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 24/07/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import Foundation
import CoreData

class BaseDBOperation: AsyncOperation {
    
    let backgroundContext: NSManagedObjectContext
    
    init(backgroundContext: NSManagedObjectContext) {
        self.backgroundContext = backgroundContext
        self.backgroundContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        super.init()
    }
}
