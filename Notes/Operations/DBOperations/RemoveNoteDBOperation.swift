//
//  RemoveNoteDBOperation.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 24/07/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import Foundation
import CoreData

class RemoveNoteDBOperation: BaseDBOperation {
    
    private let uid: String
    
    init(with uid: String, backgroundContext: NSManagedObjectContext) {
        self.uid = uid
        super.init(backgroundContext: backgroundContext)
    }
    
    override func main() {
        backgroundContext.performAndWait {
            let request = NSFetchRequest<NoteObj>(entityName: "NoteObj")
            let predicate = NSPredicate(format: "uid = %@", uid)
            request.predicate = predicate
            do {
                let noteObj = try backgroundContext.fetch(request)
                guard noteObj.count == 1 else {
                    finish()
                    return
                }
                backgroundContext.delete(noteObj[0])
                try backgroundContext.save()
                finish()
            } catch {
                finish()
            }
        }
    }
}
