//
//  LoadNotesDBOperation.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 24/07/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import UIKit
import CoreData

class LoadNotesDBOperation: BaseDBOperation {
    
    var result: [Note]?
    
    override init(backgroundContext: NSManagedObjectContext) {
        super.init(backgroundContext: backgroundContext)
    }
    
    override func main() {
        let request = NSFetchRequest<NoteObj>(entityName: "NoteObj")
        // Две строки ниже для второй версии модели
        let sortDescriptorForEditDate = NSSortDescriptor(key: "editDate", ascending: true)
        request.sortDescriptors = [sortDescriptorForEditDate]
        backgroundContext.performAndWait {
            do {
                let notesObj = try backgroundContext.fetch(request)
                var notes = [Note]()
                for noteObj in notesObj {
                    guard let note = createNote(noteObj: noteObj) else {
                        finish()
                        return
                    }
                    notes.append(note)
                }
                result = notes
                finish()
            } catch {
                finish()
            }
        }
    }
    
    private func createNote(noteObj: NoteObj) -> Note? {
        guard let uid = noteObj.uid else {
            return nil
        }
        guard let title = noteObj.title else {
            return nil
        }
        guard let content = noteObj.content else {
            return nil
        }
        guard noteObj.importance != nil, let importance = Importance(rawValue: noteObj.importance!) else {
            return nil
        }
        let colors = noteObj.color
        let color = (colors == nil) ? UIColor.white : UIColor(red: CGFloat(colors!.r),
                                                              green: CGFloat(colors!.g),
                                                              blue: CGFloat(colors!.b),
                                                              alpha: CGFloat(colors!.a))
        return Note(uid: uid,
                    title: title,
                    content: content,
                    color: color,
                    importance: importance,
                    selfDestructDate: noteObj.selfDestructDate)
    }
}
