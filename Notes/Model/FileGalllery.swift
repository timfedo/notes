import UIKit

class FileGallery {
    public private(set) var images: [UIImage]
    
    public init () {
        
        // Если используется схема Demo наполняет галлерею картинками из ассетов
        #if DEMO
        self.images = [UIImage(named: "pic1"),
                       UIImage(named: "pic2"),
                       UIImage(named: "pic3"),
                       UIImage(named: "pic4"),
                       UIImage(named: "pic5")].compactMap {$0};
        #else
        self.images = [];
        #endif
    }
    
    public func add(_ image: UIImage) {
        self.images.append(image)
    }
    
    func getCacheFileDir() -> URL {
        let path = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
        var isDir: ObjCBool = false
        let dirUrl = path.appendingPathComponent("NotebookGallery")
        if !FileManager.default.fileExists(atPath: dirUrl.path, isDirectory: &isDir), !isDir.boolValue {
            try? FileManager.default.createDirectory(at: dirUrl, withIntermediateDirectories: true, attributes: nil)
        }
        return dirUrl
    }
    
    public func saveToFile() {
        let urls = try? FileManager.default.contentsOfDirectory(at: getCacheFileDir(), includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
        guard let imagePaths = urls else { return }
        for imagePath in imagePaths {
            try? FileManager.default.removeItem(at: imagePath)
        }
        for (index, image) in images.enumerated() {
            let imageUrl = getCacheFileDir().appendingPathComponent("\(index).png")
            try? image.pngData()?.write(to: imageUrl)
        }
    }
    
    public func loadFromFile() {
        var newImages: [UIImage] = []
        let urls = try? FileManager.default.contentsOfDirectory(at: getCacheFileDir(), includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
        guard var imagePaths = urls else { return }
        imagePaths.sort(by: {$0.absoluteString.compare(
            $1.absoluteString, options: .numeric) == .orderedAscending})
        for imagePath in imagePaths {
            if let imageData: Data = try? Data(contentsOf: imagePath),
                let image: UIImage = UIImage(data: imageData) {
                newImages.append(image)
            }
        }
        if newImages.count != 0 {
            images = newImages
        }
    }
}
