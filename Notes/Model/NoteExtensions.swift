import UIKit

extension Note {
    static func parse(json: [String: Any]) -> Note? {
        let uid = json["uid"] as? String
        let title = json["title"] as? String
        let content = json["content"] as? String
        
        let color: UIColor?
        if let colorValues = json["color"] as? Array<Double> {
            color = UIColor(red: CGFloat(colorValues[0]),
                            green: CGFloat(colorValues[1]),
                            blue: CGFloat(colorValues[2]),
                            alpha: CGFloat(colorValues[3]))
        } else {
            color = nil
        }
        
        let importance: Importance?
        if let importanceRawValue = json["importance"] as? String {
            importance = Importance(rawValue: importanceRawValue)
        } else {
            importance = Importance.regular
        }
        
        let selfDestructDate: Date?
        if let timeInterval = json["selfDestructDate"] as? Double {
            selfDestructDate = Date(timeIntervalSince1970: timeInterval)
        } else {
            selfDestructDate = nil
        }
        
        switch (uid, title, content, color, importance, selfDestructDate) {
        case (_?, _?, _?, _?, _?, _):
            return Note(uid: uid!, title: title!, content: content!, color: color!,
                        importance: importance!, selfDestructDate: selfDestructDate)
        case (_?, _?, _?, nil, _?, _):
            return Note(uid: uid!, title: title!, content: content!,
                        importance: importance!, selfDestructDate: selfDestructDate)
        default:
            return nil
        }
    }
    
    var json: [String: Any] {
        var result = ["uid": self.uid, "title": self.title, "content": self.content,
                      "selfDestructDate": self.selfDestructDate?.timeIntervalSince1970 as Any] as [String: Any]
        
        if (colors != [1.0, 1.0, 1.0, 1.0]) {
            result["color"] = colors
        }
        
        if (self.importance != Importance.regular) {
            result["importance"] = self.importance.rawValue
        }
        
        return result
    }
    
    var colors: [Double] {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        self.color.getRed(&r, green: &g, blue: &b, alpha: &a)
        return [Double(r), Double(g), Double(b), Double(a)]
    }
}
