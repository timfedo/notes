import UIKit

enum Importance: String {
    case unimportant = "неважная"
    case regular = "обычная"
    case important = "важная"
}

struct Note {
    let uid: String
    let title: String
    let content: String
    let color: UIColor
    let importance: Importance
    let selfDestructDate: Date?
    
    init(uid: String = UUID().uuidString, title: String, content: String, color: UIColor = UIColor.white,
         importance: Importance, selfDestructDate: Date?) {
        self.uid = uid
        self.title = title
        self.content = content
        self.color = color
        self.importance = importance
        self.selfDestructDate = selfDestructDate
    }
}

extension Note: Equatable {
    static func == (lhs: Note, rhs: Note) -> Bool {
        return
            lhs.uid == rhs.uid &&
            lhs.title == rhs.title &&
            lhs.content == rhs.content &&
            lhs.color == rhs.color &&
            lhs.importance == rhs.importance &&
            lhs.selfDestructDate == rhs.selfDestructDate
    }
}
