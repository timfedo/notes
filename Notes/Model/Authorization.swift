//
//  Authorization.swift
//  Notes
//
//  Created by Тимофей Фёдоров on 11/08/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import Foundation

// Структкра для хранения данных для авторизации в github

struct Authorization {
    static let clientId = "37df7483fa8f67432978"
    static let clientSecret = "785e2bac62604b0b0844efba2873ada3ba7d6384"
    
    static var shared = Authorization()
    
    var login = ""
    var token = ""
    var gistId = ""
}
