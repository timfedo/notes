//
//  FileNitebook.swift
//  NotesTests
//
//  Created by Тимофей Фёдоров on 30/06/2019.
//  Copyright © 2019 timfedo. All rights reserved.
//

import XCTest
@testable import Notes

class FileNotebookTests: XCTestCase {
    
    var notebook: FileNotebook!
    
    override func setUp() {
        notebook = FileNotebook()
        super.setUp()
    }
    
    override func tearDown() {
        notebook = nil
        super.tearDown()
    }
    
    func testRemovingNonExistingUid() {
        notebook.remove(with: "1")
        XCTAssertTrue(notebook.notes.isEmpty)
    }
    
    func testSavingAndLoadingEmptyNotebook() {
        notebook.saveToFile()
        try? notebook.loadFromFile()
        XCTAssertTrue(notebook.notes.isEmpty)
    }
    
    func testNotesBeforeSavingAndAfterLoadingAreEqual() {
        let note0 = Note(uid:"1", title: "Важная заметка", content: "Эта заметка является важной",
             importance: .important, selfDestructDate: nil)
        let note1 = Note(uid:"2", title: "Обычная заметка", content: "Обычная и зелёная заметка",
                         color: .green, importance: .regular, selfDestructDate: Date(timeIntervalSince1970: 1561925767) + 3600)
        try? notebook.add(note0)
        try? notebook.add(note1)
        notebook.saveToFile()
        try? notebook.loadFromFile()
        let notes = notebook.notes
        guard !notes.isEmpty else {
            XCTFail()
            return
        }
        XCTAssertEqual(note0.uid, notes[0].uid)
        XCTAssertEqual(note0.title, notes[0].title)
        XCTAssertEqual(note0.content, notes[0].content)
        XCTAssertEqual(note0.importance, notes[0].importance)
        XCTAssertEqual(note0.color, notes[0].color)
        XCTAssertEqual(note0.selfDestructDate, notes[0].selfDestructDate)
        
        XCTAssertEqual(note1.uid, notes[1].uid)
        XCTAssertEqual(note1.title, notes[1].title)
        XCTAssertEqual(note1.content, notes[1].content)
        XCTAssertEqual(note1.importance, notes[1].importance)
        XCTAssertEqual(note1.color, notes[1].color)
        XCTAssertEqual(note1.selfDestructDate, notes[1].selfDestructDate)
    }
}
